import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Box, ImgIcon, ScaledText } from 'urip-rn-kit';

import Icons from '../../img/icon';
import Images from '../../img';
import { useNavigation } from '@react-navigation/native';

const CustAlquran = (surah) => {
    const Surah = surah.route.params.verse.verses;
    const navigation = useNavigation();

    const NamaSurah = surah.route.params.verse.name;
    
    return (
        <ScrollView>
            <View style={{flex: 1}}>
                <View 
                    style={{
                        backgroundColor: '#e67e22',
                        shadowColor: '#000',
                        shadowOffset: {
                            width: 0, 
                            height: 3,
                        },
                        shadowRadius: 5,
                        elevation: 6, 
                        flexDirection: 'row', 
                        alignItems: 'center', 
                        paddingVertical: 15,
                        paddingHorizontal: 10,
                    }}>
                    <ImgIcon onPress={() => navigation.goBack()}
                        source={Icons.back} size={15}/>
                    <ImgIcon onPress={() => navigation.goBack()}
                        source={Icons.backto} size={35}/>
                    <ScaledText style={{
                        textAlign: 'center',
                        marginHorizontal: 10,
                        color: '#fff',
                        fontWeight: 'bold',
                    }}>{NamaSurah}</ScaledText>
                </View>
            </View>
            <View>
                {Surah.map(item => {
                    return (
                        <View
                            style={{flexDirection: 'row', marginHorizontal: 10, marginVertical: 15,}} 
                            key={item.number}
                        >
                            <Box
                                backgroundImage={Images.num_bg}
                                    height={35}
                                    width={35}
                                    justifyCenter
                                    alignCenter
                            >
                                <ScaledText size={13} style={{color: '#fff',}}>{item.number}</ScaledText>
                            </Box>
                            <View style={{marginHorizontal: 10,  alignItems: 'baseline', paddingHorizontal: 25,}}>
                                <Text style={{color: '#000', fontSize: 20, paddingRight: 20}}>{item.text}</Text>
                                <Text>{item.translation_id}</Text>
                            </View>
                        </View>
                    );
                })}
            </View>
        </ScrollView>
    )
}

export default CustAlquran
