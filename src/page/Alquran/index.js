import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, StatusBar } from 'react-native';
import { Col, ImgIcon, Row, ScaledText, Box  } from 'urip-rn-kit';

import dataAlquran from '../auth/data.json';
import Icons from '../../img/icon';
import Images from '../../img';
import { SafeAreaView } from 'react-native-safe-area-context';

const Alquran = ({navigation}) => {
    const data = dataAlquran;
    return (
        <ScrollView>
            <View style={{flex: 1}}>
                <View 
                    style={{
                        backgroundColor: '#e67e22',
                        shadowColor: '#000',
                        shadowOffset: {
                            width: 0,
                            height: 3,
                        },
                        shadowRadius: 5,
                        elevation: 6, 
                        flexDirection: 'row', 
                        alignItems: 'center', 
                        paddingVertical: 15,
                        paddingHorizontal: 10,
                    }}>
                    <ImgIcon onPress={() => navigation.goBack()}
                        source={Icons.back} size={15}/>
                    <ImgIcon onPress={() => navigation.goBack()}
                        source={Icons.backto} size={35}/>
                    <Text style={{
                        textAlign: 'center',
                        marginHorizontal: 10,
                        color: '#fff',
                        fontWeight: 'bold',
                    }}>AL-QUR'AN</Text>
                </View>
            </View>
            <View>
                {data.map(item => {
                    return (
                        <TouchableOpacity 
                            key={item.number_of_surah}
                            style={{backgroundColor: '#f1f2f6', paddingVertical: 15,}}
                            onPress={() => navigation.navigate('CustAlquran', {verse: item})}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginHorizontal: 10,}}>
                                <Box
                                    backgroundImage={Images.bg}
                                    height={35}
                                    width={35}
                                    justifyCenter
                                    alignCenter
                                >
                                    <ScaledText size={13} style={{color: '#000',}}>{item.number_of_surah}</ScaledText>
                                </Box>
                                <View style={{marginHorizontal: 20,}}>
                                    <Text style={{fontSize: 20, color: '#000',}}>{item.name}</Text>
                                    <Text>{item.number_of_ayah} Ayat</Text>
                                </View>
                                <View style={{left: '100%', marginEnd: 0, justifyContent: 'flex-end', alignItems:'flex-end'}}>
                                    <Text style={{color: '#000', fontSize: 20}}>{item.name_translations.ar}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    );
                })}
            </View>
        </ScrollView>
    )
}

export default Alquran;
