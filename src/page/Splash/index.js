import React, {useEffect} from 'react';
import {View, Text, Image} from 'react-native';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    }, 2000);
  });
  return (
    <View style={{justifyContent: 'center', backgroundColor: 'white', flex: 1}}>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Image source={require('../../img/icon/icon.png')} style={{height: 160, width: 160}}>
        </Image>
        <Text
          style={{
            fontSize: 25,
            color: '#e67e22',
            fontWeight: 'bold',
            marginTop: 20,
          }}>
          ALQURAN & TAHLIL
        </Text>
      </View>
    </View>
  );
};

export default Splash;
