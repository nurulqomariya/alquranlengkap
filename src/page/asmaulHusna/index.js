import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Box, ImgIcon, ScaledText } from 'urip-rn-kit';
import dataAsmaulHusna from '../auth/db.json';
import Icons from '../../img/icon';
import Images from '../../img';

const Tahlil = ({navigation}) => {
    const data = dataAsmaulHusna;
    return (
        <ScrollView>
            <View style={{flex: 1}}>
                <View 
                    style={{
                        backgroundColor: '#e67e22',
                        shadowColor: '#000',
                        shadowOffset: {
                            width: 0,
                            height: 3,
                        },
                        shadowRadius: 5,
                        elevation: 6, 
                        flexDirection: 'row', 
                        alignItems: 'center', 
                        paddingVertical: 15,
                        paddingHorizontal: 10,
                    }}>
                    <ImgIcon onPress={() => navigation.goBack()}
                        source={Icons.back} size={15}/>
                    <ImgIcon onPress={() => navigation.goBack()}
                        source={Icons.backto} size={35}/>
                    <Text style={{
                        textAlign: 'center',
                        marginHorizontal: 10,
                        color: '#fff',
                        fontWeight: 'bold',
                    }}>ASMAUL HUSNA</Text>
                </View>
            </View>
            {data.map(item => {
                return(
                    <View 
                        style={{
                            flexDirection: 'row',
                            marginHorizontal: 10,
                            marginVertical: 15,
                            alignItems: 'center',
                        }}
                        key={item.urutan}
                    >
                        <Box
                            backgroundImage={Images.bg}
                            height={35}
                            width={35}
                            justifyCenter
                            alignCenter
                        >
                            <ScaledText size={13} style={{color: '#000',}}>{item.urutan}</ScaledText>
                        </Box>
                        <View style={{flexDirection: 'row', paddingHorizontal: 25,}}>
                            <Text style={{color: '#000', fontSize: 20, fontWeight: 'bold',}}>{item.arab}</Text>
                            <Text style={{paddingRight: 35, marginHorizontal: 10,}}>{item.arti}</Text>
                        </View>
                    </View>
                )
            })}
        </ScrollView>
    )
}

export default Tahlil;
