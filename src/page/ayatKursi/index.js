import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { ImgIcon, Box, ScaledText } from 'urip-rn-kit';

import ayatKursi from '../auth/ayatKursi.json';
import Icons from '../../img/icon';
import Images from '../../img';

const AyatKursi = ({navigation}) => {
    const data = ayatKursi;
    return (
        <ScrollView>
            <View style={{flex: 1}}>
                <View 
                    style={{
                        backgroundColor: '#e67e22',
                        shadowColor: '#000',
                        shadowOffset: {
                            width: 0,
                            height: 3,
                        },
                        shadowRadius: 5,
                        elevation: 6, 
                        flexDirection: 'row', 
                        alignItems: 'center', 
                        paddingVertical: 15,
                        paddingHorizontal: 10,
                    }}>
                    <ImgIcon onPress={() => navigation.goBack()}
                        source={Icons.back} size={15}/>
                    <ImgIcon onPress={() => navigation.goBack()}
                        source={Icons.backto} size={35}/>
                    <Text style={{
                        textAlign: 'center',
                        marginHorizontal: 10,
                        color: '#fff',
                        fontWeight: 'bold',
                    }}>AYAT KURSI</Text>
                </View>
            </View>
            {data.map(item => {
                return(
                    <View 
                        style={{
                            flexDirection: 'row',
                            marginHorizontal: 10,
                            marginVertical: 15,
                            alignItems: 'center',
                        }}
                        key={item.number}
                    >
                        <View style={{marginHorizontal: 10, paddingHorizontal: 25,}}>
                            <Text style={{color: '#000', fontSize: 20, fontWeight: 'bold',}}>{item.arabic}</Text>
                            <Text style={{color: '#555'}}>Artinya : {item.translation}</Text> 
                            <Text>Tafsir : {item.tafsir}</Text>
                        </View>
                    </View>
                )
            })}
        </ScrollView>
    )
}

export default AyatKursi;
