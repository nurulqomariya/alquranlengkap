import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'

const CustComponent = props => {
    return (
        <TouchableOpacity style={Style.line} onPress={props.onPress}>
            <Text style={{color: 'white', fontWeight: '700', fontSize: 20}}>{props.name}</Text>
        </TouchableOpacity>
    )
}

export default CustComponent;
const Style = StyleSheet.create({
    line: {
        height: 50,
        width: 250,
        margin: 7,
        borderWidth: 2,
        borderColor: '#fff',
        borderRadius: 15,
        marginHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 6,
            },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        // backgroundColor: '#e67e22',

    },
});
