import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, ImageBackground, Image, StatusBar } from 'react-native';

import CustComponent from './component';

export class Home extends Component {
  render() {
    return (
      <View>
      <StatusBar barStyle='#fffff' backgroundColor='#d35400'/>
        <ImageBackground source={require('../../img/background1.jpg')} style={{height: 900}}> 
          <Image source={require('../Splash/img1.png')} style={Styles.img}/>
          <View style={Styles.bg}>
            <CustComponent 
              name="Al-Qur'an" 
              onPress={() => this.props.navigation.navigate('Alquran')}
            />
            <CustComponent 
              name="Tahlil" 
              onPress={() => this.props.navigation.navigate('Tahlil')}
            />
            <CustComponent 
              name="Asmaul Husna" 
              onPress={() => this.props.navigation.navigate('AsmaulHusna')}
            />
            <CustComponent 
              name="Ayat Kursi" 
              onPress={() => this.props.navigation.navigate('AyatKursi')}
            />
          </View>
        </ImageBackground>
      </View>
    )
  }
}

export default Home;
const Styles = StyleSheet.create({
  bg: {
    margin: 100,
    padding: 10,
    top: -150,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
  },
  img: {
    height: 150, 
    width: 120,
    margin: 100,
    padding: 10,
    left: 40,
    top: 20,
  },
});
