import React from 'react';
import { View, Text } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Splash from '../page/Splash';
import Home from '../page/home';
import Alquran from '../page/Alquran/index';
import CustAlquran from '../page/Alquran/component';
import Tahlil from '../page/Tahlil';
import AsmaulHusna from '../page/asmaulHusna';
import AyatKursi from '../page/ayatKursi';


const Stack = createNativeStackNavigator();


const Route = () => {
    return (
        <Stack.Navigator initialRoutName="Home" screenOptions={{headerShown: false}}>
            <Stack.Screen name='Splash' component= {Splash} />
            <Stack.Screen name='Home' component= {Home} />
            <Stack.Screen name='Alquran' component= {Alquran} />
            <Stack.Screen name='CustAlquran' component= {CustAlquran} />
            <Stack.Screen name='Tahlil' component= {Tahlil} />
            <Stack.Screen name='AsmaulHusna' component= {AsmaulHusna} />
            <Stack.Screen name='AyatKursi' component= {AyatKursi} />

        </Stack.Navigator>
    )
}

export default Route;
